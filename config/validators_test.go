package config

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestValidateTitleRetrieval_GoodCase(t *testing.T) {
	trc := &TitleRetrievalConfig{
		URL:      "https://www.station-millenium.com:7000/status-json.xsl",
		Interval: "10s",
	}
	err := trc.validate()
	assert.NoError(t, err)
	assert.Equal(t, trc.IntervalDuration, 10*time.Second)
}

func TestValidateTitleRetrieval_WrongCase(t *testing.T) {
	err := (&TitleRetrievalConfig{URL: "fake-url"}).validate()
	assert.EqualError(t, err, "invalid title retrieval URL : fake-url")
}

func TestValidateTitleRetrieval_MissingInterval(t *testing.T) {
	err := (&TitleRetrievalConfig{URL: "https://www.station-millenium.com:7000/status-json.xsl"}).validate()
	assert.EqualError(t, err, "missing title retrieval interval")
}

func TestValidateTitleRetrieval_WrongInterval(t *testing.T) {
	err := (&TitleRetrievalConfig{
		URL:      "https://www.station-millenium.com:7000/status-json.xsl",
		Interval: "abc",
	}).validate()
	assert.EqualError(t, err, "time: invalid duration \"abc\"")
}

func TestValidateImageProvider_GoodCase(t *testing.T) {
	err := (&ImageProviderConfig{
		DeezerURL: "https://deezer.com",
		ItunesURL: "https://itunes.com",
	}).validate()

	assert.NoError(t, err)
}

func TestValidateImageProvider_MissingDeezerURL(t *testing.T) {
	err := (&ImageProviderConfig{
		ItunesURL: "https://itunes.com",
	}).validate()

	assert.EqualError(t, err, "missing Deezer URL")
}

func TestValidateImageProvider_MissingItunesURL(t *testing.T) {
	err := (&ImageProviderConfig{
		DeezerURL: "https://deezer.com",
	}).validate()

	assert.EqualError(t, err, "missing Itunes URL")
}

func TestValidateInfluxConfig_GoodCase(t *testing.T) {
	err := (&InfluxDBConfig{
		URL:             "http://influxdb:8086",
		Database:        "db",
		RetentionPolicy: "rp",
	}).validate()
	assert.NoError(t, err)
}

func TestValidateInfluxConfig_WrongURL(t *testing.T) {
	err := (&InfluxDBConfig{
		URL:             "fake-url",
		Database:        "db",
		RetentionPolicy: "rp",
	}).validate()
	assert.EqualError(t, err, "invalid Influx URL : fake-url")
}

func TestValidateInfluxConfig_MissingDB(t *testing.T) {
	err := (&InfluxDBConfig{
		URL:             "http://influxdb:8086",
		RetentionPolicy: "rp",
	}).validate()
	assert.EqualError(t, err, "missing Influx database")
}

func TestValidateInfluxConfig_MissingRP(t *testing.T) {
	err := (&InfluxDBConfig{
		URL:      "http://influxdb:8086",
		Database: "db",
	}).validate()
	assert.EqualError(t, err, "missing Influx retention policy")
}

func TestValidateConfigImagePath_GoodCase(t *testing.T) {
	err := (Config{
		CustomImagePath: "/tmp",
	}).validate()
	assert.NoError(t, err)
}

func TestValidateConfigImagePath_NoExistingFile(t *testing.T) {
	err := (Config{
		CustomImagePath: "/error",
	}).validate()
	assert.EqualError(t, err, "stat /error: no such file or directory")
}

func TestValidateConfigImagePath_NotADirectory(t *testing.T) {
	err := (Config{
		CustomImagePath: "/etc/fstab",
	}).validate()
	assert.EqualError(t, err, "custom image path /etc/fstab is not a directory")
}
